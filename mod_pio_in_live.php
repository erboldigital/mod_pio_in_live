<?php

// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$banner = modPioInLive::getProgram($params);
require JModuleHelper::getLayoutPath('mod_pio_in_live');