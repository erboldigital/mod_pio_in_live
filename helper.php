<?php 

class ModPioInLive {
  /**
   * Retrieves all programas
   *
   * @param   array  $params An object containing the module parameters
   *
   * @access public
   */    
  public static function getProgram($params) {
    $response = '';

    putenv('TZ=Etc/GMT+4'); 
    $mes[0]="-";
    $mes[1]="enero";
    $mes[2]="febrero";
    $mes[3]="marzo";
    $mes[4]="abril";
    $mes[5]="mayo";
    $mes[6]="junio";
    $mes[7]="julio";
    $mes[8]="agosto";
    $mes[9]="septiembre";
    $mes[10]="octubre";
    $mes[11]="noviembre";
    $mes[12]="diciembre";

    $dia[0]="Domingo";
    $dia[1]="Lunes";
    $dia[2]="Martes";
    $dia[3]="Miércoles";
    $dia[4]="Jueves";
    $dia[5]="Viernes";
    $dia[6]="Sábado";

    $gisett=(int)date("w");
    $mesnum=(int)date("m");

    $hora = date(" H:i",time());
    $fecha = date("d");
    $dato_a = date("Y");

    $h = date ("H");
    $min = date ("i");  
    //$fecha = '0';
    //$h = '11';
    //$min = '31';
    //$gisett = 0;

    // Día Lunes
    if (($dia[$gisett]=='Lunes')&&($h=='00')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_latino.jpg' >";  //MUSICAL LATINO
       }
    if (($dia[$gisett]=='Lunes')&&($h=='01'||($h<=04)&&($min<=00)))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/cierre_emision.jpg' >";  //CIERRE DE EMISION
       }
    if (($dia[$gisett]=='Lunes')&&($h=='04'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_nacional.jpg' >";     // //musical nacional  ()
       }
    if (($dia[$gisett]=='Lunes')&&($h=='05'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/informativo_qamasa.jpg' >";     // //INFORMATIVO Q'MASA  ()
       }
    if (($dia[$gisett]=='Lunes')&&($h=='06'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/qhantaty.jpg' >";    //REVISTA AYAMRA 
       }
    if (($dia[$gisett]=='Lunes')&&($h=='06')&&($min>=00)&&($min<=29))
       {
       //$response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/qhantaty.jpg' >";    //REVISTA AYAMRA 
       }
    if (($dia[$gisett]=='Lunes')&&($h=='06')&&($min>=30)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/reencuentro.jpg' >";     //REENCUENTRO                    
       }
    if (($dia[$gisett]=='Lunes')&&($h=='07'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_7am.gif' >";     //BOLIVIA EN CONTACTO                     
       }
    if (($dia[$gisett]=='Lunes')&&($h=='08')&&($min>=00)&&($min<=14))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_7am.gif' >";     //BOLIVIA EN CONTACTO                     
       }
    if (($dia[$gisett]=='Lunes')&&($h=='08')&&($min>=15)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/sin_sensura.jpg' >";     //SIN SENSURA
       }
    if (($dia[$gisett]=='Lunes')&&($h=='09')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; //NOTICIAS DE LA HORA 9:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Lunes')&&($h=='09')&&($min>=06)&&($min<=59)) 
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO     en_directo_amalia en_directo_rafael_reemplazo
       }   
    if (($dia[$gisett]=='Lunes')&&($h=='10')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >";//NOTCIAS DE LA HORA 10:00 OJO!!!!!!
       } 
    if (($dia[$gisett]=='Lunes')&&($h=='10')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO     
       }    
    if (($dia[$gisett]=='Lunes')&&($h=='11')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >";//NOTCIAS DE LA HORA 11:00 OJO!!!!!!
       } 
    if (($dia[$gisett]=='Lunes')&&($h=='11')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO     
       }
    /*rrr BORRADO NOTIHORA 12*/
    if (($dia[$gisett]=='Lunes')&&($h=='12')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_12pm.gif' >";    //BOLIVIA EN CONTACTO MEDIO DIA AUGUSTO  bolivia_en_contacto_12pm
       }   
    if (($dia[$gisett]=='Lunes')&&($h=='13'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/entre_dos_ciudades.jpg' >"; //ENTRE DOS CIUDADES
       }   
    if (($dia[$gisett]=='Lunes')&&($h=='14')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 14:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Lunes')&&($h=='14')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >"; //ERBOL DEPORTES
       }
    if (($dia[$gisett]=='Lunes')&&($h=='15')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 15:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Lunes')&&($h=='15')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Lunes')&&($h=='16')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 16:00 OJO!!!!
       }
    if (($dia[$gisett]=='Lunes')&&($h=='16')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Lunes')&&($h=='17')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 17:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Lunes')&&($h=='17')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/hablando_con_la_sole.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Lunes')&&($h=='18'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_18.gif' >";  //BOLIVIA EN CONTACTO NOCHE AUGUSTO bolivia_en_contacto_18
       }
    if (($dia[$gisett]=='Lunes')&&($h=='19'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/mi_opinion_vale.jpg' >";  //MI OPINION VALE
       }
    if (($dia[$gisett]=='Lunes')&&($h=='20')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 20:00 OJO!!!!
       }
    if (($dia[$gisett]=='Lunes')&&($h=='20')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/revista_aymara.jpg' >";  //REVISTA AYMARA
       } 
    if (($dia[$gisett]=='Lunes')&&($h=='21'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
    if (($dia[$gisett]=='Lunes')&&($h=='22'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
    if (($dia[$gisett]=='Lunes')&&($h=='23')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }

    // Día Martes
    if (($dia[$gisett]=='Martes')&&($h=='00')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_latino.jpg' >";  //MUSICAL LATINO
       }
    if (($dia[$gisett]=='Martes')&&($h=='01'||($h<=04)&&($min<=00)))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/cierre_emision.jpg' >";  //CIERRE DE EMISION
       }
    if (($dia[$gisett]=='Martes')&&($h=='04'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_nacional.jpg' >";     // //musical nacional  ()
       }
    if (($dia[$gisett]=='Martes')&&($h=='05'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/informativo_qamasa.jpg' >";     // //INFORMATIVO Q'MASA  ()
       }
    if (($dia[$gisett]=='Martes')&&($h=='06'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/qhantaty.jpg' >";    //REVISTA AYAMRA 
       }
    if (($dia[$gisett]=='Martes')&&($h=='06')&&($min>=00)&&($min<=29))
       {
       //$response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/qhantaty.jpg' >";    //REVISTA AYAMRA 
       }
    if (($dia[$gisett]=='Martes')&&($h=='06')&&($min>=30)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/reencuentro.jpg' >";     //Reencuentro                    
       }
    if (($dia[$gisett]=='Martes')&&($h=='07'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sis/bolivia_en_contacto_7am.gif' >";     //BOLIVIA EN CONTACTO                     
       }
    if (($dia[$gisett]=='Martes')&&($h=='08')&&($min>=00)&&($min<=14))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sis/bolivia_en_contacto_7am.gif' >";     //BOLIVIA EN CONTACTO                     
       }
    if (($dia[$gisett]=='Martes')&&($h=='08')&&($min>=15)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/sin_sensura.jpg' >";     //SIN SENSURA
       }
    if (($dia[$gisett]=='Martes')&&($h=='09')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; //NOTICIAS DE LA HORA 9:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Martes')&&($h=='09')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO     
       }   
    if (($dia[$gisett]=='Martes')&&($h=='10')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >";//NOTCIAS DE LA HORA 10:00 OJO!!!!!!
       } 
    if (($dia[$gisett]=='Martes')&&($h=='10')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO     
       }    
    if (($dia[$gisett]=='Martes')&&($h=='11')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >";//NOTCIAS DE LA HORA 11:00 OJO!!!!!!
       } 
    if (($dia[$gisett]=='Martes')&&($h=='11')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO     
       }
    if (($dia[$gisett]=='Martes')&&($h=='12')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_12pm.gif' >";    //BOLIVIA EN CONTACTO MEDIO DIA AUGUSTO bolivia_en_contacto_12pm
       }   
    if (($dia[$gisett]=='Martes')&&($h=='13'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/entre_dos_ciudades.jpg' >"; //ENTRE DOS CIUDADES
       }   
    if (($dia[$gisett]=='Martes')&&($h=='14')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 14:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Martes')&&($h=='14')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >"; //ERBOL DEPORTES
       }
    if (($dia[$gisett]=='Martes')&&($h=='15')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 15:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Martes')&&($h=='15')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Martes')&&($h=='16')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 16:00 OJO!!!!
       }
    if (($dia[$gisett]=='Martes')&&($h=='16')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Martes')&&($h=='17')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 17:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Martes')&&($h=='17')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Martes')&&($h=='18'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_18.gif' >";  //BOLIVIA EN CONTACTO NOCHE AUGUSTO bolivia_en_contacto_18
       }
    if (($dia[$gisett]=='Martes')&&($h=='19')&&($min>=00)&&($min<=19))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/red_guarani.jpg' >";  //RED GUARANI
       }
    if (($dia[$gisett]=='Martes')&&($h=='19')&&($min>=20)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/kuichi.jpg' >";  //KUICHI
       }
    if (($dia[$gisett]=='Martes')&&($h=='20')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 20:00 OJO!!!!
       }
    if (($dia[$gisett]=='Martes')&&($h=='20')&&($min>=05)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/revista_aymara.jpg' >";  //REVISTA AYMARA
       } 
    if (($dia[$gisett]=='Martes')&&($h=='21'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
    if (($dia[$gisett]=='Martes')&&($h=='22'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
    if (($dia[$gisett]=='Martes')&&($h=='23')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
       
    // Día Miércoles
    if (($dia[$gisett]=='Miércoles')&&($h=='00')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/el_sotano.jpg' >";  //REPRIS EL SOTANO
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='01'||($h<=04)&&($min<=00)))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/cierre_emision.jpg' >";  //CIERRE DE EMISION
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='04'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_nacional.jpg' >";     // //musical nacional  ()
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='05'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/informativo_qamasa.jpg' >";     // //INFORMATIVO Q'MASA  ()
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='06'))
       {
       $response ="<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/qhantaty.jpg' >";    //REVISTA AYAMRA 
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='06')&&($min>=00)&&($min<=29))
       {
       //$response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/qhantaty.jpg' >";    //REVISTA AYAMRA 
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='06')&&($min>=30)&&($min<=59))
       {
       //$response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/reencuentro.jpg' >";     //Reencuentro                    
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='07'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sis/bolivia_en_contacto_7am.gif' >";     //BOLIVIA EN CONTACTO                     
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='08')&&($min>=00)&&($min<=14))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sis/bolivia_en_contacto_7am.gif' >";     //BOLIVIA EN CONTACTO                     
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='08')&&($min>=15)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/sin_sensura.jpg' >";     //SIN SENSURA
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='09')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; //NOTICIAS DE LA HORA 9:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='09')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO     
       }   
    if (($dia[$gisett]=='Miércoles')&&($h=='10')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >";//NOTCIAS DE LA HORA 10:00 OJO!!!!!!
       } 
    if (($dia[$gisett]=='Miércoles')&&($h=='10')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO     
       }    
    if (($dia[$gisett]=='Miércoles')&&($h=='11')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >";//NOTCIAS DE LA HORA 11:00 OJO!!!!!!
       } 
    if (($dia[$gisett]=='Miércoles')&&($h=='11')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO   /*empleo enserio*/  
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='12')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_12pm.gif' >";    //BOLIVIA EN CONTACTO MEDIO DIA AUGUSTO
       }   
    if (($dia[$gisett]=='Miércoles')&&($h=='13'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/entre_dos_ciudades.jpg' >"; //ENTRE DOS CIUDADES
       }   
    if (($dia[$gisett]=='Miércoles')&&($h=='14')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 14:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='14')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >"; //ERBOL DEPORTES
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='15')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 15:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='15')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='16')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 16:00 OJO!!!!
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='16')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='17')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 17:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='17')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       //$response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/la_paz_somos_todos.jpg' >"; //LA PAZ SOMOS TODOS
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='18'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_18.gif' >";  //BOLIVIA EN CONTACTO NOCHE AUGUSTO
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='19')&&($min>=00)&&($min<=19))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/red_guarani.jpg' >";  //RED GUARANI
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='19')&&($min>=20)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/kuichi.jpg' >";  //KUICHI
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='20')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 20:00 OJO!!!!
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='20')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/revista_aymara.jpg' >";  //REVISTA AYMARA revista_aymara.jpg
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='21'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='22'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
    if (($dia[$gisett]=='Miércoles')&&($h=='23')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
       
    // Día Jueves
    if (($dia[$gisett]=='Jueves')&&($h=='00')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_latino.jpg' >";  //MUSICAL LATINO
       }
    if (($dia[$gisett]=='Jueves')&&($h=='01'||($h<=04)&&($min<=00)))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/cierre_emision.jpg' >";  //CIERRE DE EMISION
       }
    if (($dia[$gisett]=='Jueves')&&($h=='04'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_nacional.jpg' >";     // //MUSICAL NACIONAL  ()
       }
    if (($dia[$gisett]=='Jueves')&&($h=='05'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/informativo_qamasa.jpg' >";     // //INFORMATIVO Q'MASA  ()
       }
    if (($dia[$gisett]=='Jueves')&&($h=='06'))
       {
       //$response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/qhantaty.jpg' >";    //REVISTA AYAMRA 
       }
    if (($dia[$gisett]=='Jueves')&&($h=='06')&&($min>=00)&&($min<=29))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/qhantaty.jpg' >";    //REVISTA AYAMRA 
       }
    if (($dia[$gisett]=='Jueves')&&($h=='06')&&($min>=30)&&($min<=59))
       {
       //$response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/reencuentro.jpg' >";     //Reencuentro                    
       }
    if (($dia[$gisett]=='Jueves')&&($h=='07'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_7am.gif' >";     //BOLIVIA EN CONTACTO       
       }
    if (($dia[$gisett]=='Jueves')&&($h=='08')&&($min>=00)&&($min<=14))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_7am.gif' >";     //BOLIVIA EN CONTACTO
       }
    if (($dia[$gisett]=='Jueves')&&($h=='08')&&($min>=15)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/sin_sensura.jpg' >";     //SIN SENSURA
       }
    if (($dia[$gisett]=='Jueves')&&($h=='09')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; //NOTICIAS DE LA HORA 9:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Jueves')&&($h=='09')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO     
       }   
    if (($dia[$gisett]=='Jueves')&&($h=='10')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >";//NOTCIAS DE LA HORA 10:00 OJO!!!!!!
       } 
    if (($dia[$gisett]=='Jueves')&&($h=='10')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - AMALIA PANDO     
       }    
    if (($dia[$gisett]=='Jueves')&&($h=='11')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >";//NOTCIAS DE LA HORA 11:00 OJO!!!!!!
       } 
    if (($dia[$gisett]=='Jueves')&&($h=='11')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_amalia.jpg' >";  //EN DIRECTO - en_directo_amalia     
       }
    if (($dia[$gisett]=='Jueves')&&($h=='12')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_12pm.gif' >";    //BOLIVIA EN CONTACTO MEDIO DIA AUGUSTO
       }   
    if (($dia[$gisett]=='Jueves')&&($h=='13'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/entre_dos_ciudades.jpg' >"; //ENTRE DOS CIUDADES
       }   
    if (($dia[$gisett]=='Jueves')&&($h=='14')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 14:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Jueves')&&($h=='14')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >"; //ERBOL DEPORTES
       }
    if (($dia[$gisett]=='Jueves')&&($h=='15')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 15:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Jueves')&&($h=='15')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Jueves')&&($h=='16')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 16:00 OJO!!!!
       }
    if (($dia[$gisett]=='Jueves')&&($h=='16')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Jueves')&&($h=='17')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 17:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Jueves')&&($h=='17')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Jueves')&&($h=='18'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_18.gif' >";  //BOLIVIA EN CONTACTO NOCHE AUGUSTO
       }
    if (($dia[$gisett]=='Jueves')&&($h=='19')&&($min>=00)&&($min<=19))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/red_guarani.jpg' >";  //RED GUARANI
       }
    if (($dia[$gisett]=='Jueves')&&($h=='19')&&($min>=20)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/kuichi.jpg' >";  //KUICHI
       }
    if (($dia[$gisett]=='Jueves')&&($h=='20')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 20:00 OJO!!!!
       }
    if (($dia[$gisett]=='Jueves')&&($h=='20')&&($min>=05)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/revista_aymara.jpg' >";  //REVISTA AYMARA
       } 
    if (($dia[$gisett]=='Jueves')&&($h=='21'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
    if (($dia[$gisett]=='Jueves')&&($h=='22'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
    if (($dia[$gisett]=='Jueves')&&($h=='23')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
       
    // Día Viernes
    if (($dia[$gisett]=='Viernes')&&($h=='00')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_latino.jpg' >";  //MUSICAL LATINO
       }
    if (($dia[$gisett]=='Viernes')&&($h=='01'||($h<=04)&&($min<=00)))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/cierre_emision.jpg' >";  //CIERRE DE EMISION
       }
    if (($dia[$gisett]=='Viernes')&&($h=='04'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_nacional.jpg' >";     // //MUSICAL NACIONAL  ()
       }
    if (($dia[$gisett]=='Viernes')&&($h=='05'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/informativo_qamasa.jpg' >";     // //INFORMATIVO Q'MASA  ()
       }
    if (($dia[$gisett]=='Viernes')&&($h=='06'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/qhantaty.jpg' >";    //REVISTA AYAMRA 
       }
    if (($dia[$gisett]=='Viernes')&&($h=='06')&&($min>=30)&&($min<=59))
       {
       //$response =// "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/reencuentro.jpg' >";     //REENCUENTRO                    
       }
    if (($dia[$gisett]=='Viernes')&&($h=='07'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_7am.gif' >";     //BOLIVIA EN CONTACTO
       }
    if (($dia[$gisett]=='Viernes')&&($h=='08')&&($min>=00)&&($min<=14))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_7am.gif' >";     //BOLIVIA EN CONTACTO
       }
    if (($dia[$gisett]=='Viernes')&&($h=='08')&&($min>=15)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/especial_red_amazonica.jpg' >";     //ESPECIAL RED AMAZONICA
       }
    if (($dia[$gisett]=='Viernes')&&($h=='09')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; //NOTICIAS DE LA HORA 9:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Viernes')&&($h=='09')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >";  //EN DIRECTO - ANDRES   en_directo_andres  
       }
    if (($dia[$gisett]=='Viernes')&&($h=='10')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >";//NOTCIAS DE LA HORA 10:00 OJO!!!!!!
       } 
    if (($dia[$gisett]=='Viernes')&&($h=='10')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >";  //EN DIRECTO - ANDRES  en_directo_andres.jpg   
       }  
        //img src='index_sis/bolivia_en_contacto_7am.gif'  en_directo_pedro  en_directo_andres
       //img src='index_sis/bolivia_en_contacto_7am.gif'  
    if (($dia[$gisett]=='Viernes')&&($h=='11')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >";//NOTCIAS DE LA HORA 11:00 OJO!!!!!!
       } 
    if (($dia[$gisett]=='Viernes')&&($h=='11')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >";
       //$response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/hilo_aguja.jpg' >";  //HILO Y AGUJA   hilo_aguja.jpg   
       }
    if (($dia[$gisett]=='Viernes')&&($h=='12')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_12pm.gif' >";    //BOLIVIA EN CONTACTO MEDIO DIA AUGUSTO
       }   
    if (($dia[$gisett]=='Viernes')&&($h=='13'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/entre_dos_ciudades.jpg' >"; //ENTRE DOS CIUDADES
       }   
    if (($dia[$gisett]=='Viernes')&&($h=='14')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 14:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Viernes')&&($h=='14')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >"; //ERBOL DEPORTES
       }
    if (($dia[$gisett]=='Viernes')&&($h=='15')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 15:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Viernes')&&($h=='15')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Viernes')&&($h=='16')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 16:00 OJO!!!!
       }
    if (($dia[$gisett]=='Viernes')&&($h=='16')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/la_paz_habla_con_el_alcalde.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Viernes')&&($h=='17')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 17:00 OJO!!!!!
       }
    if (($dia[$gisett]=='Viernes')&&($h=='17')&&($min>=06)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/en_directo_pedro.jpg' >"; //EN DIRECTO CON PEDRO GEMIO
       }
    if (($dia[$gisett]=='Viernes')&&($h=='18'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_18.gif' >";  //BOLIVIA EN CONTACTO NOCHE AUGUSTO
       }
    if (($dia[$gisett]=='Viernes')&&($h=='19')&&($min>=00)&&($min<=19))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/red_guarani.jpg' >";  //RED GUARANI
       }
    if (($dia[$gisett]=='Viernes')&&($h=='19')&&($min>=20)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/kuichi.jpg' >";  //KUICHI  kuichi.jpg
       }
    if (($dia[$gisett]=='Viernes')&&($h=='20')&&($min>=00)&&($min<=05))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/noticias_hora.jpg' >"; // NOTICIAS DE LA HORA 20:00 OJO!!!!  noticias_hora.jpg
       }
    if (($dia[$gisett]=='Viernes')&&($h=='20')&&($min>=05)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/revista_aymara.jpg' >";  //REVISTA AYMARA  revista_aymara.jpg
       } 
    if (($dia[$gisett]=='Viernes')&&($h=='21'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
    if (($dia[$gisett]=='Viernes')&&($h=='22'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
    if (($dia[$gisett]=='Viernes')&&($h=='23')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical_especial.jpg' >";  //PROGRAMACION MUSICAL ESPECIAL
       }
        
    //// SABADO

    //if (($dia[$gisett]=='Sábado')&&($h=='00')&&($min>=00)&&($min<=59))
    //   {
    //   $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_latino.jpg' >";  //MUSICAL LATINO
    //   }
    if (($dia[$gisett]=='Sábado')&&($h=='00'||($h<=05)&&($min<=30)))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/cierre_emision.jpg' >";  //CIERRE DE EMISION
       }
    if (($dia[$gisett]=='Sábado')&&($h=='05')&&($min>=30)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_nacional.jpg' >";  //MUSICAL NACIONAL
       }
    if (($dia[$gisett]=='Sábado')&&($h=='06'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/encuentros.gif' >";    // ENCUENTROS
       }
    if (($dia[$gisett]=='Sábado')&&($h=='07'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_hector.jpg' >";    // BOLIVIA EN CONTACTO
       }
    if (($dia[$gisett]=='Sábado')&&($h=='08'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/yasarekomo.jpg' >";    // YASAREKOMO
       }
    if (($dia[$gisett]=='Sábado')&&($h=='09'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/semillas_del_sol.jpg' >";    // SEMILLAS DEL SOL
       }
    if (($dia[$gisett]=='Sábado')&&($h=='10'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/tejiendo_bolivia_amado.jpg' >";    // TEJIENDO BOLIVIA
       }
    if (($dia[$gisett]=='Sábado')&&($h=='11'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/tejiendo_bolivia_amado.jpg' >";    // TEJIENDO BOLIVIA
       }
    if (($dia[$gisett]=='Sábado')&&($h=='12')&&($min>=00)&&($min<=29))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/tejiendo_bolivia_amado.jpg' >";    // TEJIENDO BOLIVIA
       }     
    if (($dia[$gisett]=='Sábado')&&($h=='12')&&($min>=30)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/bolivia_en_contacto_3.gif' >";    // BOLIVIA EN CONTACTO
       }
    if (($dia[$gisett]=='Sábado')&&($h=='13')&&($min>=00)&&($min<=29))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/informativo_beni_pando.jpg' >";    // INFORMATIVO BENI PANDO
       }
    if (($dia[$gisett]=='Sábado')&&($h=='13')&&($min>=30)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/kantuju.jpg' >";    // KANTUJU
       }
    if (($dia[$gisett]=='Sábado')&&($h=='14')&&($min>=00)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/kantuju.jpg' >";    // KANTUJU
       }
    if (($dia[$gisett]=='Sábado')&&($h=='15'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >";    // ERBOL DEPORTES
       }
    if (($dia[$gisett]=='Sábado')&&($h=='16'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >";    // ERBOL DEPORTES
       }
     if (($dia[$gisett]=='Sábado')&&($h=='17'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >";    // ERBOL DEPORTES
       }
    if (($dia[$gisett]=='Sábado')&&($h=='18'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/voces_nuestras.jpg' >";    // VOCES NUESTRAS
       }
    if (($dia[$gisett]=='Sábado')&&($h=='19'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/markasa.jpg' >";    // MARKASA 
       }
    if (($dia[$gisett]=='Sábado')&&($h=='20'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/markasa.jpg' >";    // MARKASA
       }
    if (($dia[$gisett]=='Sábado')&&($h=='21'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/markasa.jpg' >";    // MARKASA
       }  
    //if (($dia[$gisett]=='Sábado')&&($h=='22'))
    //   {
    //   $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/el_sotano.jpg' >";    // EL SOTANO
    //   }    
    //if (($dia[$gisett]=='Sábado')&&($h=='23')&&($min>=00)&&($min<=59))
    if (($dia[$gisett]=='Sábado')&&($h=='22'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical.jpg' >";  //PROGRAMACION MUSICAL
       }      
    if (($dia[$gisett]=='Sábado')&&($h=='23'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical.jpg' >";  //PROGRAMACION MUSICAL
       }      
    ///DOMINGO

    //if (($dia[$gisett]=='Domingo')&&($h=='00')&&($min>=00)&&($min<=59))
    //   {
    //   $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_latino.jpg' >";  //MUSICAL LATINO
    //   }
    if (($dia[$gisett]=='Domingo')&&($h=='00'||($h<=05)))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/cierre_emision.jpg' >";  //CIERRE DE EMISION
       }
    if (($dia[$gisett]=='Domingo')&&($h=='06')&&($min>=44))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_latino.jpg' >";  // MUSICAL CRISTIANO
       }
    if (($dia[$gisett]=='Domingo')&&($h=='06')&&($min>=45)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/un_tal_jesus.jpg' >";  // UN TAL JESUS
       }
    if (($dia[$gisett]=='Domingo')&&($h=='07'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/santa_misa.jpg' >";    // MISA
       } 
    if (($dia[$gisett]=='Domingo')&&($h=='08'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/radar_juvenil.jpg' >";    // RADAR JUVENIL
       }
    if (($dia[$gisett]=='Domingo')&&($h=='09'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/andando.jpg' >";    // ANDANDO
       }
    if (($dia[$gisett]=='Domingo')&&($h=='10'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/hagamos_democracia_nancy.jpg' >";    // HAGAMOS DEMOCRACIA
       }
    if (($dia[$gisett]=='Domingo')&&($h=='11'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/hagamos_democracia_nancy.jpg' >";    // HAGAMOS DEMOCRACIA
       }
    if (($dia[$gisett]=='Domingo')&&($h=='12'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/hilo_aguja.jpg' >";    // HILO Y AGUJA
       }
    if (($dia[$gisett]=='Domingo')&&($h=='13'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/musical_nacional.jpg' >";    // MUSICAL NACIONAL
       }  
    if (($dia[$gisett]=='Domingo')&&($h=='14'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >";    // ERBOL DEPORTES 
       }
    if (($dia[$gisett]=='Domingo')&&($h=='15'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >";    // ERBOL DEPORTES 
       }
    if (($dia[$gisett]=='Domingo')&&($h=='16'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >";    // ERBOL DEPORTES 
       }
    if (($dia[$gisett]=='Domingo')&&($h=='17'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >";    // ERBOL DEPORTES
       }    
    if (($dia[$gisett]=='Domingo')&&($h=='18')&&($min>=00)&&($min<=29))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/erbol_deportes.gif' >";    // ERBOL DEPORTES
       }    
    if (($dia[$gisett]=='Domingo')&&($h=='18')&&($min>=30)&&($min<=59))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/club_del_recuerdo.jpg' >";    // CLUB DEL RECUERDO 
       }   
    if (($dia[$gisett]=='Domingo')&&($h=='19'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/club_del_recuerdo.jpg' >";    // CLUB DEL RECUERDO 
       }   
    if (($dia[$gisett]=='Domingo')&&($h=='20'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/club_del_recuerdo.jpg' >";    // CLUB DEL RECUERDO 
       }   
    if (($dia[$gisett]=='Domingo')&&($h=='21'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/club_del_recuerdo.jpg' >";    // CLUB DEL RECUERDO
       }
    //   if (($dia[$gisett]=='Domingo')&&($h=='22'))
    //   {
    //   $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/podium_neerlandes.jpg' >";    // PODIUM NEERLANDES
    //   }
    if (($dia[$gisett]=='Domingo')&&($h=='22'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical.jpg' >";  //PROGRAMACION MUSICAL
       } 
    //if (($dia[$gisett]=='Domingo')&&($h=='23')&&($min>=00)&&($min<=59))
    if (($dia[$gisett]=='Domingo')&&($h=='23'))
       {
       $response = "<img src='http://www.erbol.com.bo/sites/default/files/index_sys/programacion/programacion_musical.jpg' >";  //PROGRAMACION MUSICAL
       }

    return $response;
  }
}